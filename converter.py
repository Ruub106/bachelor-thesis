#!/usr/bin/python3
#Program to convert .gz files to .txt files that only contain the location- and word labels of the tweets.

import os
import subprocess 

def list_files():
    """Make list of all .gz files"""
    f_list = [] #list of files
    for f in os.listdir("."):
        if f.endswith(".gz"):
            f_list.append(f)
    return f_list
 
def filter_file(f_list):
    """Filtering of location- and word labels with the use of tweet2tab. Convert .gz file to .txt file"""
    for number,f in enumerate(f_list):        
        subprocess.call("zcat %s | /net/corpora/twitter2/tools/tweet2tab user.location words > %s.txt" % (f,number), shell = True)

def main():
    f_list = list_files()
    filtered_tweets = filter_file(f_list)
        
if __name__ == '__main__':
	main()
