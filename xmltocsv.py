#!/usr/bin/python3
#Program to convert .xml file to .csv file.

import xml.etree.ElementTree as ET
import csv
                    
def main():
    tree = ET.parse("nl-sentiment.xml")
    root = tree.getroot()

    csv_file = open("lexicon.csv","w")
    csvwriter = csv.writer(csv_file)
    for word in root.iter("word"):
        sentiment = []
        form = word.get("form")
        sentiment.append(form)
        polarity = word.get("polarity")
        sentiment.append(polarity) 
        csvwriter.writerow(sentiment)    
    csv_file.close()     
       		
if __name__ == '__main__':
	main()
