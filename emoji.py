#!/usr/bin/python3
#Program to analyze the sentiment of the emojis in the tweets.

import os
import re
import pandas as pd
from collections import Counter

def list_files():
    """Make a list of all the txt files in the directory"""
    f_list=[] 
    for f in os.listdir("."):
        if f.endswith(".txt"):
            f_list.append(f)
    return f_list

def location(f1,f2,dic_places):
    """Removing lines that do not contain proper Dutch location"""
    p = set()    
    for line in f1:
        if line not in p:
            p.add(line)
            location,text = line.split("\t")
            for key in dic_places:
                dic2_places = dic_places[key]                
                if location in dic2_places: 
                    newl = dic2_places[location]
                    f2.write('{0}\t{1}'.format(str(newl),text))
                else:
                    if location.count(",") == 1: #check location like Amsterdam, Nederland or Gelderland, Apeldoorn
                        l1,l2 = location.split(",")
                        if l1 in dic2_places:
                            newl = dic2_places[l1]
                            f2.write('{0}\t{1}'.format(str(newl),text))
                        elif l2 in dic2_places:
                            newl = dic2_places[l2]
                            f2.write('{0}\t{1}'.format(str(newl),text))
                        elif location.count(",") == 2: #location like Amsterdam, Centrum, Nederland or Noord, Nederland, Assen
                            l1,l2,l3 = location.split(",")
                            if l1 in dic2_places:
                                newl = dic2_places[l1]
                                f2.write('{0}\t{1}'.format(str(newl),text))
                            elif l2 in dic2_places:
                                newl = dic2_places[l2]
                                f2.write('{0}\t{1}'.format(str(newl),text))
                            elif l3 in dic2_places:
                                newl = dic2_places[l3]
                                f2.write('{0}\t{1}'.format(str(newl),text))
 
def emoji(f5,f6,pos_emoji,neg_emoji,neu_emoji):
    p = set()    
    for line in f5:
        if line not in p:
            p.add(line)
            location,text = line.split("\t")
            tot_c = 0
            ls1=[]
            for word in text:
                if word in ls1:
                    tot_c += 0
                elif word in pos_emoji:
                    ls1.append(word)
                    tot_c += 10
                elif word in neg_emoji:
                    ls1.append(word)
                    tot_c -= 10
                elif word in neu_emoji:
                    ls1.append(word)
                    tot_c += 1
            f6.write('{0}\t{1}\n'.format(location,tot_c))

def test(f3,f4,pos_emoji,neg_emoji,neu_emoji):
    for line in f3:
        location,text = line.split("\t")
        for word in text:
            for char in word:
                if char in pos_emoji or char in neg_emoji or char in neu_emoji:
                    f4.write('{0}\t{1}'.format(location,text))    


def dictionary_scores(f7):
    """dictionary with for each province all the sentiment scores of the tweets"""
    dic_scores={}
    for line in f7:
        location,score = line.rstrip().split("\t")
        if location in dic_scores.keys():
            dic_scores[location].append(score)
        else:
            dic_scores[location] = [score]
    return dic_scores

def dictionary_average(dic_scores):
    """dictionary with average sentiment score of all tweets for each province"""
    dic_average_score = {}
    for k in dic_scores:
        dic_scores[k] = list(map(float,dic_scores[k]))
        dic_average_score[k] = (int(sum(dic_scores[k])))/len(dic_scores[k])
    return dic_average_score

def concatenate_dics(list_dic_average_score):
    """Concatenate all the dictionary to one dictionary"""
    c=Counter()
    for dic in list_dic_average_score:
        c.update(dic)
    return c


def main():
    list_dic_average_score = []
    f_list = list_files()            
    for f in f_list:
        with open(f,"r") as f1, open("location.txt","w") as f2:
            dic_places = pd.read_excel('municipalities.xlsx', index_col=0).to_dict() #dictionary with place as key and the municipality as value            
            location(f1,f2,dic_places)  

        with open('location.txt', 'r') as f3, open("emoji.txt","w") as f4:
  
            pos_emoji=["😀","😁","😂","🤣","😃","😄","😅","😆","😉","😊","😋","😎","😍","😘","😗","😙","😚","☺","🙂","🤗","🤩"]
            neg_emoji=["☹","🙁","😖","😞","😟","😤","😢","😭","😦","😧","😨","😩","🤯","😬","😰","😱","😳","🤪","😵","😡","😠","🤬"]
            neu_emoji=["🤔","🤨","😐","😑","😶","🙄","😏","😣","😥","😮","🤐","😯","😪","😫","😴","😌","😛","😜","😝","🤤","😒","😓","😔","😕","🙃","🤑","😲"]

            probeer = test(f3,f4,pos_emoji,neg_emoji,neu_emoji)

        with open('emoji.txt', 'r') as f5, open("score.txt","w") as f6:
            e = emoji(f5,f6,pos_emoji,neg_emoji,neu_emoji)
  
    
        with open("score.txt","r") as f7:
            dic_scores = dictionary_scores(f7)
            dic_average_score = dictionary_average(dic_scores)
            list_dic_average_score.append(dic_average_score.copy())
    
    sum_dic = concatenate_dics(list_dic_average_score)
    print(dict(sum_dic))
        
    
if __name__ == '__main__':
	main()
